# Proyecto CDIO 2019/2020

## Alumnos:
* Juan Sala Ferrera
* Alejandro Vañó Tomás
* Daniel Poquet Ramírez
* Víctor García Espert
* Nacho Sandalinas García

## Contenido  
  
Proyecto de Arduino de la asignatura CDIO en la Universidad Politécnica  
de Valencia Campus de Gandia.  
  
* Sensor de humedad
* Sensor de salinidad
* Sensor de temperatura
* Sensor de luz
* Acelerómetro
* GPS
* Envio de datos mediante WiFi
