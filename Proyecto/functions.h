  /**
  * @Function startAds - SetUp app
  * @param {pointer} ads - Pointer of ads object needed to initialize
  * @param {int} power_pin - Pin needed to send a high pulse
  * @return {void} - This function don't return nothing is only app configuration
  */
void startAds(Adafruit_ADS1115* ads, int power_pin) {
  //SetUp app
  Serial.println("Inicializando...");
  ads->begin(); // Object initialization
  pinMode(power_pin, OUTPUT); // Set pin 5 as output
  Serial.println("Ajustando la ganancia...");
  ads->setGain(GAIN_ONE); // Set Gain is GAIN_ONE because that sensor works with 3,3V
  Serial.println("Setup realizado...");
}


void connectWiFi()
{
  byte ledStatus = LOW;

  // #ifdef PRINT_DEBUG_MESSAGES
  //   Serial.print("MAC: ");
  //   Serial.println(WiFi.macAddress());
  // #endif
  
  WiFi.begin(WiFiSSID, WiFiPSK);

  while (WiFi.status() != WL_CONNECTED)
  {
    // Blink the LED
    digitalWrite(POWER_PIN, ledStatus); // Write LED high/low
    ledStatus = (ledStatus == HIGH) ? LOW : HIGH;
    #ifdef PRINT_DEBUG_MESSAGES
       Serial.println(".");
    #endif
    delay(500);
  }
  #ifdef PRINT_DEBUG_MESSAGES
     Serial.println( "WiFi Connected" );
     Serial.println(WiFi.localIP()); // Print the IP address
  #endif
}


void HTTPPost(String fieldData[], int numFields){

// Esta funcion construye el string de datos a enviar a ThingSpeak mediante el metodo HTTP POST
// La funcion envia "numFields" datos, del array fieldData.
// Asegurate de ajustar numFields al número adecuado de datos que necesitas enviar y activa los campos en tu canal web
  
    if (client.connect( Server_Host , Server_HttpPort )){
       
        // Construimos el string de datos. Si tienes multiples campos asegurate de no pasarte de 1440 caracteres
   
        String PostData= "api_key=" + MyWriteAPIKey ;
        for ( int field = 1; field < (numFields + 1); field++ ){
            PostData += "&field" + String( field ) + "=" + fieldData[ field ];
        }     
        
        // POST data via HTTP
        #ifdef PRINT_DEBUG_MESSAGES
            Serial.println( "Connecting to ThingSpeak for update..." );
        #endif
        client.println( "POST http://" + String(Rest_Host) + "/update HTTP/1.1" );
        client.println( "Host: " + String(Rest_Host) );
        client.println( "Connection: close" );
        client.println( "Content-Type: application/x-www-form-urlencoded" );
        client.println( "Content-Length: " + String( PostData.length() ) );
        client.println();
        client.println( PostData );
        #ifdef PRINT_DEBUG_MESSAGES
            Serial.println( PostData );
            Serial.println();
            //Para ver la respuesta del servidor
            #ifdef PRINT_HTTP_RESPONSE
              delay(500);
              Serial.println();
              while(client.available()){String line = client.readStringUntil('\r');Serial.print(line); }
              Serial.println();
              Serial.println();
            #endif
        #endif
    }
}


void HTTPGet(String fieldData[], int numFields){
// Esta funcion construye el string de datos a enviar a ThingSpeak o Dweet mediante el metodo HTTP GET
// La funcion envia "numFields" datos, del array fieldData.
// Asegurate de ajustar "numFields" al número adecuado de datos que necesitas enviar y activa los campos en tu canal web
   Serial.print("Función HTTP");
   Serial.println(Server_Host);
   Serial.print("HttpPort");
   Serial.println(Server_HttpPort);
    if (client.connect( Server_Host , Server_HttpPort )){
           #ifdef REST_SERVER_THINGSPEAK 
              String PostData= "GET https://api.thingspeak.com/update?api_key=";
              PostData= PostData + MyWriteAPIKey ;
           #else 
              String PostData= "GET http://dweet.io/dweet/for/";
              PostData= PostData + MyWriteAPIKey +"?" ;
           #endif
           
           for ( int field = 1; field < (numFields +1); field++ ){
              PostData += "&field" + String( field ) + "=" + fieldData[ field ];
           }
          
           
           #ifdef PRINT_DEBUG_MESSAGES
              Serial.println( "Connecting to Server for update..." );
           #endif
           client.print(PostData);         
           client.println(" HTTP/1.1");
           client.println("Host: " + String(Rest_Host)); 
           client.println("Connection: close");
           client.println();
           #ifdef PRINT_DEBUG_MESSAGES
              Serial.println( PostData );
              Serial.println();
              //Para ver la respuesta del servidor
              #ifdef PRINT_HTTP_RESPONSE
                delay(500);
                Serial.println();
                while(client.available()){String line = client.readStringUntil('\r');Serial.print(line); }
                Serial.println();
                Serial.println();
              #endif
           #endif  
    }
}

// Función espera 1s para leer del GPS
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while(ss1.available())
    {
      gps1.encode(ss1.read());  // leemos del gps
    }
  } while(millis() - start < ms);
}

// Función para encender/apagar mediante un pulso
void switch_on_off()
{
   // Power on pulse
  digitalWrite(INIT_PIN,LOW);
  delay(200);
  digitalWrite(INIT_PIN,HIGH);
  delay(200); 
  digitalWrite(INIT_PIN,LOW);
 }

 void gpsSetup(){
  ss1.begin(GPS_BAUD); // Inicializar la comunicación con el GPS
 
  pinMode(INIT_PIN,OUTPUT); 
  switch_on_off(); // Pulso para encender el GPS

  Serial.println("Fecha      Hora       Latitud   Longitud   Alt    Rumbo   Velocidad");
  Serial.println("(MM/DD/YY) (HH/MM/SS)     (deg)       (deg)  (ft)                   (mph)");
  Serial.println("-------------------------------------------------------------------------"); 
}
void gpsGetInfo(TinyGPSPlus gps){
  char gpsDate[10]; 
  char gpsTime[10];

  if(gps.location.isValid()){ // Si el GPS está recibiendo los mensajes NMEA

    sprintf(gpsDate,"%d/%d/%d", gps.date.month(),gps.date.day(),gps.date.year()); // Construimos string de datos fecha
    sprintf(gpsTime,"%d/%d/0%d", gps.time.hour(),gps.time.minute(),gps.time.second());  // Construimos string de datos hora

    Serial.print(gpsDate);
    Serial.print('\t');
    Serial.print(gpsTime);
    Serial.print('\t');
    Serial.print(gps.location.lat(),6);
    Serial.print('\t');
    Serial.print(gps.location.lng(),6);
    Serial.print('\t');
    Serial.print(gps.altitude.feet());
    Serial.print('\t');
    Serial.print(gps.course.deg(),2);
    Serial.print('\t');
    Serial.println(gps.speed.mph(),2);
  }
  else  // Si no recibe los mensajes
  {
    
    Serial.print("Satellites in view: ");
    Serial.println(gps.satellites.value());
  }
  smartDelay(1000);
}

/* acelerometer functions */
//Funcion auxiliar lectura
void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t *Data)
{
   Wire.beginTransmission(Address);
   Wire.write(Register);
   Wire.endTransmission();

   Wire.requestFrom(Address, Nbytes);
   uint8_t index = 0;
   while (Wire.available())
      Data[index++] = Wire.read();
}

// Funcion auxiliar de escritura
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
   Wire.beginTransmission(Address);
   Wire.write(Register);
   Wire.write(Data);
   Wire.endTransmission();
}
void acelerometerConfig(int acelerometerAddress)
{
   Serial.println("Configurando acelerómetro...");
   // Configurar acelerometro
   I2CwriteByte(acelerometerAddress, 28, ACC_FULL_SCALE_16_G);
}

void acelerometerWakeOnMotionConfig(int acelerometerAddress)
{

   // Configurar wake on montion del acelerometro

   I2CwriteByte(acelerometerAddress, 0x6B, 0);
   I2CwriteByte(acelerometerAddress, 0x6C, 7);
   I2CwriteByte(acelerometerAddress, 0x1D, 9);
   I2CwriteByte(acelerometerAddress, 0x38, 64);
   I2CwriteByte(acelerometerAddress, 0x69, 192);
   I2CwriteByte(acelerometerAddress, 0x1F, 10); // de 0 a 255 8 bits
   I2CwriteByte(acelerometerAddress, 0x1E, 3);  // de 0 a 16 4bits
   I2CwriteByte(acelerometerAddress, 0x37, 128);
   I2CwriteByte(acelerometerAddress, 0x6B, 32);

   uint8_t intStatus[1];
   I2Cread(acelerometerAddress, 0x3A, 1, intStatus); //leemos de intStatus para desactivar la interrupción
}

void interruptionConfig(int interruptionPin)
{
   pinMode(interruptionPin, INPUT_PULLUP);
   // Asociamos la interrupcion con el pin, con la funcion y con el umbral
   attachInterrupt(digitalPinToInterrupt(interruptionPin), handleInterrupt, CHANGE);
}

void handleInterrupt()
{
   uint8_t intStatus[1];
   I2Cread(AcelerometerInterruptionAddress, 0x3A, 1, intStatus); //leemos de intStatus para desactivar la interrupción
}