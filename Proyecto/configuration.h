/* External libraries */
#include <TinyGPS++.h>
#include <Wire.h> //Library that allows you to communicate with I2C
#include <Adafruit_ADS1015.h>//Library that allows you to use ADS
#include <math.h> //Math library
#include <ESP8266WiFi.h> //Library to connect Wireless



#include <SoftwareSerial.h>

/* Adafruit Object */
Adafruit_ADS1115 ads(0x48); // ADS Constructor
/* Constants needed to calculate temperature */
const double B = -0.79;
const double M = 0.03;
const double E = 2.71828;
const double POW = pow(E,B);

/* Another variables */
const int POWER_PIN = 5; // Digital I/O pin, Global variable
const int SLEEP_TIME = 500;

/* Calibrated parameters of salinity functions */ 
const int minSalinity = 1100;
const int maxSalinity = 21500;

/* Calibrated parameters of humidity functions */
const int airValueEXT = 20000;
const int waterValueEXT = 11000;

/* Sensors variables */
#define SALINITY_FIELD 1
#define HUMIDITY_FIELD 2
#define TEMPERATURE_FIELD 3
#define LUMINITY_FIELD 4
/* Wireless variables */

// Comentar/Descomentar para ver mensajes de depuracion en monitor serie y/o respuesta del HTTP server
#define PRINT_DEBUG_MESSAGES
//#define PRINT_HTTP_RESPONSE

// Comentar/Descomentar para conexion Fuera/Dentro de UPV
#define WiFi_CONNECTION_UPV

#define PRINT_HTTP_RESPONSE
// Selecciona que servidor REST quieres utilizar entre ThingSpeak y Dweet
#define REST_SERVER_THINGSPEAK //Selecciona tu canal para ver los datos en la web (https://thingspeak.com/channels/360979)

/* Conecction variables*/
#ifdef WiFi_CONNECTION_UPV //Conexion UPV
  const char WiFiSSID[] = "GTI1";
  const char WiFiPSK[] = "1PV.arduino.Toledo";
  // const char WiFiSSID[] = "visoren-ru";
  // const char WiFiPSK[] = "@gandia19visorenru";
#else
  const char WiFiSSID[] = "visoren-ru";
  const char WiFiPSK[] = "@gandia19visorenru"
#endif
#if defined(WiFi_CONNECTION_UPV) //Conexion UPV
  const char Server_Host[] = "proxy.upv.es";
  const int Server_HttpPort = 8080;
  //   const char Server_Host[] = "api.thingspeak.com";
  // const int Server_HttpPort = 80;
#else
  const char Server_Host[] = "api.thingspeak.com";
  const int Server_HttpPort = 80;
#endif
/*Wifi object */
WiFiClient client;

#ifdef REST_SERVER_THINGSPEAK 
  const char Rest_Host[] = "api.thingspeak.com";
  String MyWriteAPIKey="9AWLJY5HMD2Z40QB"; // Escribe la clave de tu canal ThingSpeak
#endif

#define NUM_FIELDS_TO_SEND 4 //Fields to sent to server

// /* GPS variables */
#define RX_PIN  12 // GPS RXI
#define TX_PIN  13 // GPS TX0
#define INIT_PIN 15 // Pin para  Inicializar el GPS

#define GPS_BAUD  4800  //  velocidad de comunicación serie 

TinyGPSPlus gps1; // Definimos el objeto gps

SoftwareSerial ss1(RX_PIN,TX_PIN); // Creamos una UART software para comunicación con el GPS

String variables[NUM_FIELDS_TO_SEND + 1];
/* Internal libraries */

/* acelerometer variables */
#define MPU9250_ADDRESS 0x68
#define AcelerometerInterruptionAddress 0x68
#define MAG_ADDRESS 0x0C
#define interruptPin 4

#define GYRO_FULL_SCALE_250_DPS 0x00
#define GYRO_FULL_SCALE_500_DPS 0x08
#define GYRO_FULL_SCALE_1000_DPS 0x10
#define GYRO_FULL_SCALE_2000_DPS 0x18

#define ACC_FULL_SCALE_2_G 0x00
#define ACC_FULL_SCALE_4_G 0x08
#define ACC_FULL_SCALE_8_G 0x10
#define ACC_FULL_SCALE_16_G 0x18

void ICACHE_RAM_ATTR handleInterrupt();

#include "functions.h" // Library with basics functions
/* Libraries with sensors functions */
#include "./sensorsFunctions/salinityFunction.h"
#include "./sensorsFunctions/humidityFunction.h"
#include "./sensorsFunctions/temperatureFunction.h"
#include "./sensorsFunctions/luminityFunction.h"
