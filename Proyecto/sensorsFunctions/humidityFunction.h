  /**
  * @Function readHumidity - Function needed to read % of humidity
  * @param {int} airValueEXT - Parameter calibrated with dry humidity sensor
  * @param {int} waterValueEXT - Parameter calibrated with wet humidity sensor
  * @param {int} ADCpin - ADS pin needed to read
  * @param {pointer} - Pointer of Adafruit object
  * @return {void} At the moment don't return nothing, later return a int, to save data into database
  */
String readHumidity(int airValueEXT, int waterValueEXT ,int ADCpin, Adafruit_ADS1115* adafruit) {

  double humidity;
  double adc0 = adafruit->readADC_SingleEnded(ADCpin); // Read humidity value
  humidity = 100 * airValueEXT / (airValueEXT - waterValueEXT) - adc0 * 100 / (airValueEXT - waterValueEXT);
  if(humidity < 0){
    humidity = 0;
  }else if(humidity > 100){
    humidity = 100;
  }
  Serial.print("Humedad (%): ");
  Serial.print(humidity);
  Serial.println("%");

  return String(humidity);
}
