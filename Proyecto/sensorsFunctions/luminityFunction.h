  /**
  * @Function readLuminity - Function needed to read luminity
  * @param {int} pin - ADS pin needed to read
  * @param {pointer} - Pointer of Adafruit object
  * @return {void} At the moment don't return nothing, later return a int, to save data into database
  */

  String readLuminity(int pin, Adafruit_ADS1115* ads){
      double res = ads->readADC_SingleEnded(pin);
      if(res <= 150){
        Serial.println("Sensor luminosidad: Sin luz");
      }else if(res <= 3400){
        Serial.println("Senosr luminosidad: Luz interior");
      }else if(res <= 8500){
        Serial.println("Sensor luminosidad: Exterior con sombra");
      }else{
        Serial.println("Sensor luminosidad: Exterior soleado");
      }
      return String(res);
  }