  /**
  * @Function readTemperature - Function needed to read ºC
  * @param {int} pin - ADS pin needed to read
  * @param {pointer} - Pointer of Adafruit object
  * @return {void} At the moment don't return nothing, later return a int, to save data into database
  */
String readTemperature(int pin, Adafruit_ADS1115* ads){
    double vo = map(ads->readADC_SingleEnded(pin), 0,32767, 0, 4096);
    double vo2 = (vo/1000);
    double temperature = ((vo2 * POW)/M);

    Serial.print("Temperatura (ºC): ");
    Serial.print(temperature);
    Serial.println("ºC");
    return String(temperature);
}