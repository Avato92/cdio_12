  /**
  * @Function salinityReader - Set salinity average value
  * @param {int} mins - Minimum salinity value
  * @param {int} maxs - Maximum salinity value
  * @param {int} pin - ADS pin needed to read
  * @param {pointer} - Pointer of Adafruit object
  * @return {void} At the moment don't return nothing, later return a int, to save data into database
  */
double salinityAverage(int mins, int maxs, int pin, Adafruit_ADS1115* adafruit){
  int total = 0;
  // Loop to set salinity average
  for(int i = 0; i < 20; i++){
    total = total + adafruit->readADC_SingleEnded(pin);
  }
  int average = total / 20;
  // Map to set % of salinity
  double salinity = map(average, mins, maxs, 0 , 100);
    if(salinity < 0){
    salinity = 0;
  }else if(salinity > 100){
    salinity = 100;
  }
  Serial.print("Salinidad (%): ");
  Serial.print(salinity);
  Serial.println("%");
  return salinity;
}
  /**
  * @Function readSalinity - Function to read ADS pin
  * @param {int} powerPin - Pin needed to send a high pulse
  * @param {int} min - Minimum salinity value
  * @param {int} max - Maximum salinity value
  * @param {int} pin - ADS pin needed to read
  * @param {pointer} - Pointer of Adafruit object
  * @return {void} At the moment don't return nothing, later return a int, to save data into database
  */
String readSalinity(int powerPin, int min, int max, int pin, Adafruit_ADS1115* adafruit) {
  int adc1; // Pin A1 connect to salinity sensor
  digitalWrite( powerPin, HIGH ); // Turn on sensor
  double res = salinityAverage(min, max, pin, adafruit);
  digitalWrite( powerPin, LOW ); // Turn off power
  return String(res);
}
