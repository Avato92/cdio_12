#include "configuration.h" // Library with all the constants used by the configuration system
////////////////////////// SETUP  /////////////////////////////////////////////////
void setup() {
  Serial.begin(9600);
  startAds(&ads, POWER_PIN); //Initialize ads
  connectWiFi();
  digitalWrite(POWER_PIN, HIGH);
  gpsSetup();
  interruptionConfig(interruptPin);
  acelerometerConfig(MPU9250_ADDRESS);
  acelerometerWakeOnMotionConfig(MPU9250_ADDRESS);
}

///////////////////////// LOOP ///////////////////////////////////////////////////
void loop() {
  gpsGetInfo(gps1);
  /* Pin to read */
  int ADCpin = 0;
  /* Finally, call function */
  variables[SALINITY_FIELD] = readSalinity(POWER_PIN, minSalinity, maxSalinity, ADCpin, &ads);
  /* change PIN */
  ADCpin = 1;
  /* Call function */
  variables[HUMIDITY_FIELD] = readHumidity(airValueEXT, waterValueEXT, ADCpin, &ads);
  /* Necessary pin for the temperature function and call function */ 
  ADCpin = 2;
  variables[TEMPERATURE_FIELD] = readTemperature(ADCpin, &ads);
  /* Function to read luminity */
  ADCpin = 3;
  variables[LUMINITY_FIELD]=readLuminity(ADCpin, &ads);
  //Serial.println("antes del get");
  HTTPGet( variables, NUM_FIELDS_TO_SEND );
  // Sleep arduino 5 seconds
  ESP.deepSleep(SLEEP_TIME * 1000000);
 //delay(5000);
}
